<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 03.06.2016
 * Time: 09:34
 */
namespace Ortnit\Lib\System;

//use Ortnit\Lib\System\Path;

class Process
{
    protected $os = '';
    protected $clk_tck = 100;

    public function __construct()
    {
        $this->os = PHP_OS;
        $result = intval(exec('getconf CLK_TCK'));
        if($result > 0) {
            $this->clk_tck = $result;
        }

    }

    public function getAllProcesses() {
        $processes = [];
        if (substr($this->os, 0, 3) == 'Win') {
        } elseif ($this->os == 'Linux') {
            $processes = $this->_getAllLinuxProcesses();
        }
        return $processes;
    }

    protected function _getAllLinuxProcesses() {
        $processes = [];
        $procDirectory = '/proc';
        $dir = opendir($procDirectory);
        while(($node = readdir($dir)) !== false) {
            //@TODO exe and cwd link path
            //@TODO implement is_dir in the check
            //@TODO use joinPath
            if(is_numeric($node)) {
                $path = Path::joinPath($procDirectory, $node);

                $cmdLinePath = Path::joinPath($path, 'cmdline');
                $cmdLine = '';
                if(is_file($cmdLinePath)) {
                    $cmdLine = file_get_contents($cmdLinePath);
                    $cmdLine = trim(str_replace("\0", ' ', $cmdLine));
                }

                $stats = $this->getLinuxProcessStats($node);

                $kernelThread = false;
                if(empty($cmdLine)) {
                    $kernelThread = true;
                }

                $processes[$node] = [
                    'cmdline' => $cmdLine,
                    'stats' => $stats,
                    'kernelThread' => $kernelThread
                ];
            }
        }
        return $processes;
    }

    protected function getLinuxProcessStats($pid) {
        $stats = [];
        $statsPath = sprintf('/proc/%d/stat', $pid);
        if(is_file($statsPath)) {
            $content = file_get_contents($statsPath);

            $leftPos = strpos($content, '(') + 1;
            $rightPos = strrpos($content, ')');
            $comm = substr($content, $leftPos, $rightPos - $leftPos);
            $content = trim(substr($content, 0, $leftPos - 1) . substr($content, $rightPos + 2));

            $contentArray = explode(' ', $content);
            if(count($contentArray) == 51) {
                $stats = [
                    'pid' => $contentArray[0],
                    'comm' => $comm,
                    'state' => $contentArray[1],
                    'ppid' => $contentArray[2],
                    'pgrp' => $contentArray[3],
                    'session' => $contentArray[4],
                    'tty_nr' => $contentArray[5],
                    'tpgid' => $contentArray[6],
                    'flags' => $contentArray[7],
                    'minflt' => $contentArray[8],
                    'cminflt' => $contentArray[9],
                    'majflt' => $contentArray[10],
                    'cmajflt' => $contentArray[11],
                    'utime' => $contentArray[12],
                    'stime' => $contentArray[13],
                    'cutime' => $contentArray[14],
                    'cstime' => $contentArray[15],
                    'priority' => $contentArray[16],
                    'nice' => $contentArray[17],
                    'num_threads' => $contentArray[18],
                    'itrealvalue' => $contentArray[19],
                    'starttime' => $contentArray[20],
                    'vsize' => $contentArray[21],
                    'rss' => $contentArray[22],
                    'rsslim' => $contentArray[23],
                    'startcode' => $contentArray[24],
                    'endcode' => $contentArray[25],
                    'startstack' => $contentArray[26],
                    'kstkesp' => $contentArray[27],
                    'kstkeip' => $contentArray[28],
                    'signal' => $contentArray[29],
                    'blocked' => $contentArray[30],
                    'sigignore' => $contentArray[31],
                    'sigcatch' => $contentArray[32],
                    'wchan' => $contentArray[33],
                    'nswap' => $contentArray[34],
                    'cnswap' => $contentArray[35],
                    'exit_signal' => $contentArray[36],
                    'processor' => $contentArray[37],
                    'rt_priority' => $contentArray[38],
                    'policy' => $contentArray[39],
                    'delayacct_blkio_ticks' => $contentArray[40],
                    'guest_time' => $contentArray[41],
                    'cguest_time' => $contentArray[42],
                    'start_data' => $contentArray[43],
                    'end_data' => $contentArray[44],
                    'start_brk' => $contentArray[45],
                    'arg_start' => $contentArray[46],
                    'arg_end' => $contentArray[47],
                    'env_start' => $contentArray[48],
                    'env_end' => $contentArray[49],
                    'exit_code' => $contentArray[50],
                ];
            }
        }
        return $stats;
    }


}