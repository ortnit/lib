<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 06.06.2016
 * Time: 10:26
 */

namespace Ortnit\Lib\Set;

class Set implements \Iterator
{
    protected $_set = [];
    protected $_iteratorPosition = 0;

    public function __construct($array = null) {
        if(!is_array($array) || $array !== null) {
            throw new \Exception('first parameter has to be an array');
        }

        if(is_array($array)) {
            foreach ($array as $value) {
                $this->addValue($value);
            }
        }
    }

    public function addValue($value) {
        if($this->hasValue($value) === false) {
            $this->_set[] = $value;
        }
    }

    public function getValue($value) {
        if(($index = $this->hasValue($value)) !== false) {
            unset($this->_set[$index]);
            return $value;
        }
        return null;
    }

    public function hasValue($value) {
        return array_search($value, $this->_set);
    }

    public function add(Set $set) {

        return $this;
    }

    public function sub(Set $set) {

        return $this;
    }

    public function cut(Set $set) {

        return $this;
    }

    public function toArray() {
        return $this->_set;
    }

    /**
     * \Iterator Interface
     */

    public function current()
    {
        return current($this->_set);
    }

    public function key()
    {
        return key($this->_set);
    }

    public function next()
    {
        return next($this->_set);
    }

    public function rewind()
    {
        reset($this->_set);
        return key($this->_set);
    }

    public function valid()
    {
        return ($this->current() !== false)?true:false;
    }
}